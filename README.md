# Descargar

Puede descargar la ultima versión de login.py desde el [repositorio](https://bitbucket.org/haskmell/login.py/raw/f84d0140d37b19205b6ef57ff8bb987be74a3cec/login.py)


# Uso

Para usar el programa solo abra  una terminal y ejecute el programa con python:

        python login.py
       
El programa le solicitará sus datos de login la primera vez que lo ejecute y las
próximas veces lo logeará automáticamente. El script va a tratar de establecer
conexión con la red ocupando los datos proporcionados.

Si todo sale bien debería decir: "Entramos"

# Configuración

Si por alguna razón necesita restablecer sus datos de acceso, ejecute el programa con
la opción '-r'

	python login.py -r

y se le pedirán los datos nuevamente.

Es necesario aclarar que estos datos son locales y se guardan el su carpeta principal
'/home/[usuario]/.login\_usach'.


# Errores

En caso de encontrar un error, [reportalo][iss] en español.

[iss]: https://bitbucket.org/haskmell/login.py/issues?status=new&status=open


# Desarrollo

El desarrollo principal de este repositorio se encuentra en:

[https://bitbucket.org/haskmell/login.py/overview](https://bitbucket.org/haskmell/login.py/overview)
        
Si desea contribuir con nuevas características o correcciones de errores, debe tener una cuenta
en bitbucket.org, hacer un fork (copia) del repositorio, realizar sus
modificaciones y luego hacer un "Pull Request" para que sus cambios sean
añadidos al repositorio principal.

Más información puede ser encontrada en el manual de bitbucket:

* [Fork](https://confluence.atlassian.com/display/BITBUCKET/Forking+a+Repository)
* [Pull Request](https://confluence.atlassian.com/display/BITBUCKET/Working+with+pull+requests)
